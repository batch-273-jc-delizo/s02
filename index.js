/*
QUIZ
1. Spaghetti Code
2. Object literals are written using curly braces {} in JavaScript. An object literal is a comma-separated list of key-value pairs, where the key is a string (or a symbol in ES6+) that acts as an identifier for the value.
3. OOP
4. studentOne.enroll();
5. True
6. { key: value }
7. TRUE
8. TRUE
9. TRUE
10. TRUE
*/


// ACTIVITY

// 1. 
let studentOne = {
    name: 'John',
    email: 'john@mail.com',
    grades: [89, 84, 78, 88],
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        this.grades.forEach(grade => {
            console.log(grade);
        })
    },
    computeQuarterlyAverage() {
        const sum = this.grades.reduce((acc, curr) => acc + curr, 0);
        const avg = sum / this.grades.length;
        return avg;
    },
    willPass() {
        const average = this.computeQuarterlyAverage();
        if (average >= 85) {
            return true;
        } else {
            return false;
        }
    },
    willPassWithHonors() {
        const avg = this.computeQuarterlyAverage();
        if (avg >= 90) {
            return true;
        } else if (avg >= 85 && avg < 90) {
            return false;
        } else {
            return undefined;
        }
    }
};

let studentTwo = {
    name: 'Joe',
    email: 'joe@mail.com',
    grades: [78, 82, 79, 85],
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        this.grades.forEach(grade => {
            console.log(grade);
        })
    },
    computeQuarterlyAverage() {
        const sum = this.grades.reduce((acc, curr) => acc + curr, 0);
                const avg = sum / this.grades.length;
        return avg;
    },
    willPass() {
        const average = this.computeQuarterlyAverage();
        if (average >= 85) {
            return true;
        } else {
            return false;
        }
    },
    willPassWithHonors() {
        const avg = this.computeQuarterlyAverage();
        if (avg >= 90) {
            return true;
        } else if (avg >= 85 && avg < 90) {
            return false;
        } else {
            return undefined;
        }
    }
};

let studentThree = {
    name: 'Jane',
    email: 'jane@mail.com',
    grades: [87, 89, 91, 93],
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        this.grades.forEach(grade => {
            console.log(grade);
        })
    },
    computeQuarterlyAverage() {
        const sum = this.grades.reduce((acc, curr) => acc + curr, 0);
        const avg = sum / this.grades.length;
        return avg;
    },
    willPass() {
        const average = this.computeQuarterlyAverage();
        if (average >= 85) {
            return true;
        } else {
            return false;
        }
    },
    willPassWithHonors() {
        const avg = this.computeQuarterlyAverage();
        if (avg >= 90) {
            return true;
        } else if (avg >= 85 && avg < 90) {
            return false;
        } else {
            return undefined;
        }
    }
};

let studentFour = {
    name: 'Jessie',
    email: 'jessie@mail.com',
    grades: [91, 89, 92, 93],
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        this.grades.forEach(grade => {
            console.log(grade);
        })
    },
    computeQuarterlyAverage() {
        const sum = this.grades.reduce((acc, curr) => acc + curr, 0);
        const avg = sum / this.grades.length;
        return avg;
    },
    willPass() {
        const average = this.computeQuarterlyAverage();
        if (average >= 85) {
            return true;
        } else {
            return false;
        }
    },
    willPassWithHonors() {
        const avg = this.computeQuarterlyAverage();
        if (avg >= 90) {
            return true;
        } else if (avg >= 85 && avg < 90) {
            return false;
        } else {
            return undefined;
        }
    }
};

console.log(studentOne)
console.log(studentTwo)
console.log(studentThree)
console.log(studentFour)

// 2.

console.log(`${studentOne.name}'s quarterly average is ${studentOne.computeQuarterlyAverage()}`);
console.log(`${studentTwo.name}'s quarterly average is ${studentTwo.computeQuarterlyAverage()}`);
console.log(`${studentThree.name}'s quarterly average is ${studentThree.computeQuarterlyAverage()}`);
console.log(`${studentFour.name}'s quarterly average is ${studentFour.computeQuarterlyAverage()}`);

// 3. 

console.log(`${studentOne.name} will pass: ${studentOne.willPass()}`);
console.log(`${studentTwo.name} will pass: ${studentTwo.willPass()}`);
console.log(`${studentThree.name} will pass: ${studentThree.willPass()}`);
console.log(`${studentFour.name} will pass: ${studentFour.willPass()}`);

// 4. 

console.log(`${studentOne.name} will pass with flying colors: ${studentOne.willPassWithHonors()}`);
console.log(`${studentTwo.name} will pass with flying colors: ${studentTwo.willPassWithHonors()}`);
console.log(`${studentThree.name} will pass with flying colors: ${studentThree.willPassWithHonors()}`);
console.log(`${studentFour.name} will pass with flying colors: ${studentFour.willPassWithHonors()}`);

// 5. 

let classOf1A = {
  students: [studentOne, studentTwo, studentThree, studentFour],

  countHonorStudents() {
    let count = 0;

    for (let student of this.students) {
      if (student.willPassWithHonors()) {
        count++;
      }
    }

    return count;
  },
  
  // calculate the percentage of honor students
  honorsPercentage() {
    let totalStudents = this.students.length;
    let honorStudents = this.countHonorStudents();
    let percentage = (honorStudents / totalStudents) * 100;
    return percentage;
  },
    retrieveHonorStudentInfo() {
        let honorStudents = [];
        this.students.forEach(student => {
            if (student.willPassWithHonors()) {
                honorStudents.push({
                	name: student.name,
                    email: student.email,
                    averageGrade: student.computeQuarterlyAverage()
                });
            }
        });
        return honorStudents;
    },
  sortHonorStudentsByGradeDesc() {
    let honorStudents = this.students.filter(student => student.willPassWithHonors());
    honorStudents.sort((a, b) => b.computeQuarterlyAverage() - a.computeQuarterlyAverage());
    let sortedHonorStudents = honorStudents.map(student => {
      return { email: student.email, averageGrade: student.computeQuarterlyAverage() };
    });
    return sortedHonorStudents;
  }
};



console.log(classOf1A)

// 6. 

console.log(`The number of honor students in classOf1A is ${classOf1A.countHonorStudents()}`);

// 7. 

console.log(`The percentage of honor students in classOf1A is ${classOf1A.honorsPercentage()}%`);

// 8. 

const honorStudents = classOf1A.retrieveHonorStudentInfo();
console.log(`The following students are honor students in classOf1A:`);
honorStudents.forEach(student => {
  console.log(`- ${student.name}: Average Grade - ${student.averageGrade}`);
});

// 9.

console.log("Sorting honor students by grade in descending order:");
console.log(classOf1A.sortHonorStudentsByGradeDesc());
